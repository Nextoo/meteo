package nxt.weather;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
    
    /**
     * Lance l'application Spring
     * @param args
     */
    public static void main(String[] args) {
	SpringApplication.run(Application.class, args);
    }
    
}
